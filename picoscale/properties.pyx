# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

include "smaract_h.pxi"

from code cimport Code

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2020, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

cdef class _Property(Code)  # abstract intermediate class

cdef class InstrumentProperty(_Property):
    _valid_values = {
        SA_SI_PROTOCOL_VERSION_PROP:
            ["PROTOCOL_VERSION", "protocol version"],
        SA_SI_PROTOCOL_VERSION_STRING_PROP:
            ["PROTOCOL_VERSION_STRING", "protocol version string"],
        SA_SI_DEVICE_TYPE_PROP:
            ["DEVICE_TYPE", "device type"],
        SA_SI_DEVICE_ID_PROP:
            ["DEVICE_ID", "device id"],
        SA_SI_DEVICE_SERIAL_NUMBER_PROP:
            ["DEVICE_SERIAL_NUMBER", "device serial number"],
        SA_SI_DEVICE_NAME_PROP:
            ["DEVICE_NAME", "device name"],
        SA_SI_NUMBER_OF_FIRMWARE_VERSIONS_PROP:
            ["NUMBER_OF_FIRMWARE_VERSIONS", "number of firmware versions"],
        SA_SI_FIRMWARE_VERSION_PROP:
            ["FIRMWARE_VERSION", "firmware version"],
        SA_SI_FIRMWARE_VERSION_STRING_PROP:
            ["FIRMWARE_VERSION_STRING", "firmware version string"],
        SA_SI_MAX_DATA_OBJECT_CHUNK_SIZE_PROP:
            ["MAX_DATA_OBJECT_CHUNK_SIZE", ""],
        SA_SI_NUMBER_OF_CHANNELS_PROP:
            ["NUMBER_OF_CHANNELS", "number of channels"],
        SA_SI_MAX_FRAME_RATE_PROP:
            ["MAX_FRAME_RATE", "maximum frame rate"],
        SA_SI_FRAME_RATE_PROP:
            ["FRAME_RATE", "frame rate"],
        SA_SI_MAX_FRAME_AGGREGATION_PROP:
            ["MAX_FRAME_AGGREGATION", "maximum frame rate aggregation"],
        SA_SI_FRAME_AGGREGATION_PROP:
            ["FRAME_AGGREGATION", "frame aggregation"],
        SA_SI_FRAME_INDEX_ENABLED_PROP:
            ["FRAME_INDEX_ENABLED", "frame index enabled"],
        SA_SI_PRECISE_FRAME_RATE_PROP:
            ["PRECISE_FRAME_RATE", "precise frame rate"],
        SA_SI_EVENT_NOTIFICATION_ENABLED_PROP:
            ["EVENT_NOTIFICATION_ENABLED", "event notification enabled"],
        SA_SI_STREAMING_ACTIVE_PROP:
            ["STREAMING_ACTIVE", "streaming active"],
        SA_SI_STREAMING_MODE_PROP:
            ["STREAMING_MODE", "streaming mode"],
    }

cdef class ChannelProperty(_Property):
    _valid_values = {
        SA_SI_NUMBER_OF_DATA_SOURCES_PROP:
            ["NUMBER_OF_DATA_SOURCES", "number of data sources"],
        SA_SI_CHANNEL_NAME_PROP:
            ["CHANNEL_NAME", "channel name"],
    }

cdef class DataSourceProperty(_Property):
    _valid_values = {
        SA_SI_DATA_SOURCE_TYPE_PROP:
            ["DATA_SOURCE_TYPE", "data source type"],
        SA_SI_DATA_TYPE_PROP:
            ["DATA_TYPE", "data type"],
        SA_SI_AVAILABLE_COMPRESSION_MODES_PROP:
            ["AVAILABLE_COMPRESSION_MODES", "available compression modes"],
        SA_SI_COMPRESSION_MODE_PROP:
            ["COMPRESSION_MODE", "compression mode"],
        SA_SI_STREAMING_ENABLED_PROP:
            ["STREAMING_ENABLED", "streaming enabled"],
        SA_SI_BASE_UNIT_PROP:
            ["BASE_UNIT", "base unit"],
        SA_SI_BASE_RESOLUTION_PROP:
            ["BASE_RESOLUTION", "base resolution"],
        SA_SI_RESOLUTION_SHIFT_PROP:
            ["RESOLUTION_SHIFT", "resolution shift"],
        SA_SI_DATA_SOURCE_NAME_PROP:
            ["DATA_SOURCE_NAME", "data source name"],
        SA_SI_IS_STREAMABLE_PROP:
            ["IS_STREAMABLE", "is streamable"],
        SA_SI_COMPONENT_ID_PROP:
            ["COMPONENT_ID", "component id"],
        SA_SI_COMPONENT_INDEX_PROP:
            ["COMPONENT_INDEX", "component index"],
    }
