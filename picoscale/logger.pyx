# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

import logging
from logging import handlers
from multiprocessing import current_process
from os import access, W_OK, path, makedirs, getuid
from pwd import getpwuid
from threading import currentThread


__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2020, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


_logger_NOTSET = logging.NOTSET  # 0
_logger_CRITICAL = logging.CRITICAL  # 50
_logger_ERROR = logging.ERROR  # 40
_logger_WARNING = logging.WARNING  # 30
_logger_INFO = logging.INFO  # 20
_logger_DEBUG = logging.DEBUG  # 10


cdef class Logger:

    # class attribute definitions in the pxd file
    _level_as_str = {_logger_NOTSET:   '',
                     _logger_CRITICAL: 'CRITICAL',
                     _logger_ERROR:    'ERROR',
                     _logger_WARNING:  'WARNING',
                     _logger_INFO:     'INFO',
                     _logger_DEBUG:    'DEBUG'}

    def __init__(self, name="Logger", logger_name=None,
                 logging_folder=None, logging_file=None,
                 debug=None, *args, **kwargs):
        super(Logger, self).__init__()
        # store constructor parameters
        self.__name = name
        self.__logger_name = logger_name or "Picoscale"
        self.__set_logging_folder(logging_folder)
        self.__logging_file = logging_file or "Picoscale"
        # build the internal references
        self.__logger_obj = logging.getLogger(self.__logger_name)
        if debug is True:
            self.log_level = _logger_DEBUG
        else:
            self.log_level = _logger_INFO
        if not len(self.__logger_obj.handlers):
            # at least have one handler if it is not already defined
            self.__handler_obj = handlers.RotatingFileHandler(
                self.logging_file_fullname, maxBytes=1e6, backupCount=5)
            formatter = logging.Formatter(
                '%(asctime)s - %(levelname)-7s - %(name)s - %(message)s')
            self.__handler_obj.setFormatter(formatter)
            self.__logger_obj.addHandler(self.__handler_obj)
        else:
            self.__handler_obj = self.__logger_obj.handlers[0]

    @property
    def name(self):
        return self.__name

    def _rename(self, new_name):
        self.debug("from now on, renamed to '{}'".format(new_name))
        self.__name = new_name

    @property
    def _process_id(self):
        return current_process().name

    @property
    def _thread_id(self):
        return currentThread().getName()

    @property
    def logging_folder(self):
        return self.__logging_folder

    def __set_logging_folder(self, folder):
        if folder is not None:
            try:
                if not path.exists(folder):
                    makedirs(folder)
                if access(folder, W_OK):
                    self.__logging_folder = folder
                    return True
            except Exception as exc:
                self.error("Exception setting logging folder to {}: {}"
                           "".format(folder, exc))
            return False
        else:
            folder_candidates = ['/var/log', '/tmp/log']
            for folder in folder_candidates:
                if self.__set_logging_folder(folder):
                    return True  # it worked
            # last try is the home of the user
            folder = getpwuid(getuid()).pw_dir
            return self.__set_logging_folder(folder)

    @property
    def logging_file(self):
        return self.__logging_file

    @property
    def logging_file_fullname(self):
        return "{}/{}.log".format(self.logging_folder, self.logging_file)

    @property
    def logger_obj(self):
        return self.__logger_obj

    @property
    def handler_obj(self):
        return self.__handler_obj

    def add_handler(self, handler):
        self.__logger_obj.addHandler(handler)

    def remove_handler(self, handler=None):
        if handler is not None:
            self.__logger_obj.removeHandler(handler)
        else:
            self.__logger_obj.removeHandler(self.__handler_obj)

    def replace_handler(self, handler):
        self.remove_handler()
        self.add_handler(handler)
        self.__handler_obj = handler

    @property
    def log_level(self):
        return self._logger_obj.level

    @log_level.setter
    def log_level(self, value):
        self.__logger_obj.setLevel(value)

    def log_message(self, msg, level):
        _tag = self._level_as_str[level]
        method = {_logger_CRITICAL: self.__logger_obj.critical,
                  _logger_ERROR: self.__logger_obj.error,
                  _logger_WARNING: self.__logger_obj.warning,
                  _logger_INFO: self.__logger_obj.info,
                  _logger_DEBUG: self.__logger_obj.debug}
        method[level]("{0}: {1}".format(self.name, msg))

    def critical(self, msg, *args):
        try:
            if len(args) > 0:
                msg = msg.format(*args)
            self.log_message(msg, _logger_CRITICAL)
        except Exception as exc:
            self.log_message("Cannot log {0!r} because {1}".format(msg, exc),
                             _logger_CRITICAL)

    def error(self, msg, *args):
        try:
            if len(args) > 0:
                msg = msg.format(*args)
            self.log_message(msg, _logger_ERROR)
        except Exception as exc:
            self.log_message("Cannot log {0!r} because {1}".format(msg, exc),
                             _logger_CRITICAL)

    def warning(self, msg, *args):
        try:
            if len(args) > 0:
                msg = msg.format(*args)
            self.log_message(msg, _logger_WARNING)
        except Exception as exc:
            self.log_message("Cannot log {0!r} because {1}".format(msg, exc),
                             _logger_CRITICAL)

    def info(self, msg, *args):
        try:
            if len(args) > 0:
                msg = msg.format(*args)
            self.log_message(msg, _logger_INFO)
        except Exception as exc:
            self.log_message("Cannot log {0!r} because {1}".format(msg, exc),
                             _logger_CRITICAL)

    def debug(self, msg, *args):
        try:
            if len(args) > 0:
                msg = msg.format(*args)
            self.log_message(msg, _logger_DEBUG)
        except Exception as exc:
            self.log_message("Cannot log {0!r} because {1}".format(msg, exc),
                             _logger_CRITICAL)

