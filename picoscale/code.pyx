# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

include "code.pxi"

from . cimport logger

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2020, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

cdef class Code(logger.Logger):
    """ TODO: describe
    """
    def __init__(self, int code *args, **kwargs):
        """TODO: document
        """
        super(Code, self).__init__(*args, **kwargs)
        self.__set_value(code)

    def __int__(self):
        return self.__code_number

    def __str__(self):
        return self.__human_readable

    def __repr__(self):
        return f"{int(self)}:{str(self)}"

    def __richcmp__(self, other, int op):
        if op in [2, 3]:  # ==, !=
            if int(self) == int(other):
                return True if op == 2 else False
            return False if op == 2 else True
        return False

    @property
    def code(self):
        return self.__code_number

    @code.setter
    def code(self, value):
        self.__set_value(value)

    def __set_value(self, value):
        if value in self.valid_codes():
            self.__code_number = value
            self.__code_str, self.__human_readable = self._interpret(
                self.__code_number)
        else:
            raise ValueError("Invalid value {} assignment".format(value))

    @property
    def string(self):
        return "{}".format(self.__code_str)

    @property
    def human(self):
        return "{}".format(self.__human_readable)

    def _interpret(self, value):
        """
Method to be implemented on the subclasses and must return an array with two
elements: an integer with the code and a string with the meaning.
        """
        if not hasattr(self, "_valid_values"):
            raise NotImplementedError(
                "Not implemented in abstract superclass!")
        return self._valid_values.get(
            value, [None, "Unknown {} ({})".format(self.__class__.__name__,
                                                   self.__code_number)])

    def valid_values(self):
        """
Method to be implemented on the subclasses and must return a dictionary with
codes in the keys and each item a list with the code as string in the first
element, followed by the meaning.

        """
        if not hasattr(self, "_valid_values"):
            raise NotImplementedError(
                "Not implemented in abstract superclass!")
        return self._valid_values

    def valid_codes(self):
        return list(self._valid_values.keys())

    def valid_strings(self):
        if self._valid_strings is None:
            self._valid_strings = []
            for code in self._valid_values.keys():
                self._valid_strings.append(self._valid_values[code][0])
        return self._valid_strings[:]
