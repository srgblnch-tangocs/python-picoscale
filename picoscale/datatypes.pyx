# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

include "smaract_h.pxi"

from code cimport Code

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2020, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

cdef class CompressModes(Code):
    _valid_values = {
        SA_SI_NO_COMPRESSION_MODE: ["NO_COMPRESSION", "no compression"],
        SA_SI_LOWER16ABS_COMPRESSION_MODE: ["LOWER16ABS_COMPRESSION",
                                            "lower bits absolute"],
    }

cdef class DataType(Code):
    _valid_values = {
        SA_SI_INT8_DTYPE: ["INT8", "1 byte signed integer"],
        SA_SI_UINT8_DTYPE: ["UINT8", "1 byte unsigned integer"],
        SA_SI_INT16_DTYPE: ["INT16", "2 bytes signed integer"],
        SA_SI_UINT16_DTYPE: ["UINT16", "2 bytes unsigned integer"],
        SA_SI_INT32_DTYPE: ["INT32", "4 bytes signed integer"],
        SA_SI_UINT32_DTYPE: ["UINT32", "4  bytes signed integer"],
        SA_SI_INT48_DTYPE: ["INT48", "6 bytes signed integer"],
        SA_SI_UINT48_DTYPE: ["UINT48", "6 bytes unsigned integer"],
        SA_SI_INT64_DTYPE: ["INT64", "8 bytes signed integer"],
        SA_SI_UINT64_DTYPE: ["UINT64", "8 bytes unsigned integer"],
        SA_SI_FLOAT32_DTYPE: ["FLOAT32", "4 bytes float"],
        SA_SI_FLOAT64_DTYPE: ["FLOAT64", "8 bytes float"],
        SA_SI_STRING_DTYPE: ["STRING", "set of byte chars"],
    }

cdef class Units(Code):
    _valid_values = {
        SA_SI_NO_UNIT: ["NO_UNIT", "no unit"],
        SA_SI_PERCENT_UNIT: ["PERCENT", "%%"],
        SA_SI_METRE_UNIT: ["METRE", "m"],
        SA_SI_DEGREE_UNIT: ["DEGREE", "deg"],
        SA_SI_SECOND_UNIT: ["SECOND", "s"],
        SA_SI_HERTZ_UNIT: ["HERTZ", "Hz"],
        SA_SI_KILOGRAM_UNIT: ["KILOGRAM", "Kg"],
        SA_SI_NEWTON_UNIT: ["NEWTON", "N"],
        SA_SI_WATT_UNIT: ["WATT", "W"],
        SA_SI_JOULE_UNIT: ["JOULE", "J"],
        SA_SI_VOLT_UNIT: ["VOLT", "V"],
        SA_SI_AMPERE_UNIT: ["AMPERE", "A"],
        SA_SI_OHM_UNIT: ["OHM", "Ohm"],
        SA_SI_PASCAL_UNIT: ["PASCAL", "Pa"],
        SA_SI_KELVIN_UNIT: ["KELVIN", "K"],
        SA_SI_DEGREE_CELSIUS_UNIT: ["DEGREE_CELSIUS", "ºC"],
        SA_SI_SQUARE_METRE_UNIT: ["SQUARE_METER", "m^2"],
        SA_SI_METRE_PER_SECOND_UNIT: ["METRE_PER_SECOND", "m/s"],
        SA_SI_METRE_PER_SQUARE_SECOND_UNIT: ["METRE_PER_SQUARE_SECOND",
                                             "m/s^2"],
    }
