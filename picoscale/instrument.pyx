# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

include "instrument.pxi"

import socket

from datatypes cimport (CompressModes, DataType, Units)
from errors cimport Error
from logger cimport Logger
from properties cimport _Property
from properties cimport InstrumentProperty, ChannelProperty, DataSourceProperty
from streaming_mode cimport StreamingMode

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2020, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


cdef class Picoscale(Logger):

    cdef:
        __serial_number
        __ip_address
        __port
        __locator
        SA_SI_Handle __handle
        __is_open
        __instrument_id
        __nchannels
        __n_data_sources_per_channel
        __streaming_mode

    def __cinit__(self):
        pass

    def __init__(self, serial_number=None, ip_address=None, port=None,
                 *args, **kwargs):
        """TODO: document
        """
        super(Picoscale, self).__init__(*args, **kwargs)
        self.__serial_number = None
        self.__ip_address = None
        self.__port = None
        self.__build_locator(serial_number, ip_address, port)
        self.__is_open = False
        self.__do_instrument_open()

    def __del__(self):
        """TODO: document
        """
        if self.is_open():
            self.close()

    def __dealloc__(self):
        """TODO: document
        """
        if self.is_open():
            self.close()

    def __str__(self):
        """TODO: document
        """
        return self.__locator

    def __repr__(self):
        """TODO: document
        """
        return self.__locator

    def __build_locator(self, serial_number=None, ip_address=None, port=None):
        """
locator: From the documentation, locator are strings similar to URLs
First element defines
examples: usb:ix:0                    -> device 0 in the usb
          usb:sn:<serial>             -> usb connected instrument with <serial>
          network:192.168.1.100:55555 -> network connected instrument
        """
        if serial_number is not None:
            self.debug("building a locator based on the serial number {}"
                       "".format(serial_number))
            self.__serial_number = int(serial_number)
            self.__locator = str.encode(
                "usb:sn:{}".format(self.__serial_number))
        elif ip_address is not None and self.__validate_ip_address(ip_address):
            # FIXME: validate is it a valid ipv4 (or ipv6)
            if port is None:
                port = DEFAULT_PORT
            else:
                port = int(port)
            self.__port = port
            if isinstance(ip_address, bytes):
                ip_address = ip_address.decode()
            self.debug("building a locator based on the network {}:{}"
                       "".format(ip_address, port))
            self.__ip_address = ip_address
            self.__locator = str.encode(
                "network:{}:{}".format(self.__ip_address, self.__port))
        else:
            raise AssertionError("It couldn't build the locator with the "
                                 "provided information")
        self.info("locator: {}".format(self.__locator.decode()))
        self._rename(self.__locator.decode())

    @property
    def locator(self):
        return self.__locator

    @property
    def serial_number(self):
        return self.__serial_number

    @property
    def ip_address(self):
        return self.__ip_address

    @property
    def port(self):
        return self.__port

    def __validate_ip_address(self, address):
        # TODO: accept dns names
        if isinstance(address, bytes):
            address = address.decode()
        if self.__is_ipv4(address) or self.__is_ipv6(address):
            return True
        return False

    def __is_ipv4(self, address):
        try:
            socket.inet_pton(socket.AF_INET, address)
            self.debug("{} is recognized as an ipv4".format(address))
            return True
        except Exception as exc:
            try:
                socket.inet_aton(address)
            except Exception as exc:
                self.debug("{} exception in ipv4 inet_aton: {}"
                           "".format(address, exc))
                return False
            if address.count('.') != 3:
                self.debug("{} hasn't the 3 '.' for ipv4".format(address))
                return False
            else:
                try:
                    for each in address.split('.'):
                        value = int(each)
                        if 0 > value > 255:
                            self.debug("{} has {} out of the range of ipv4"
                                       "".format(address, value))
                            raise AssertionError("octet out of range")
                except Exception as exc:
                    self.debug("{} exception in split: {}"
                               "".format(address, exc))
                    return False
        self.debug("{} is recognized as an ipv4".format(address))
        return True

    def __is_ipv6(self, address):
        try:
            socket.inet_pton(socket.AF_INET6, address)
        except Exception as exc:  # socket.error:  # not a valid address
            self.debug("{} exception in ipv6 inet_pton: {}"
                       "".format(address, exc))
            return False
        self.debug("{} is recognized as an ipv6".format(address))
        return True

    def is_open(self):
        return self.__is_open is True

    def open(self):
        self.__do_instrument_open()

    def close(self):
        self.__do_instrument_close()

    def __do_instrument_open(self):
        cdef:
            unsigned int result
        if self.is_open():
            self.warning("Open try when it is open")
            return
        self.debug("going to call Open to the locator {}"
                   "".format(self.__locator))
        result = SA_SI_Open(&self.__handle, self.__locator, "")
        if result != SA_SI_OK:
            # TODO: check the return code to report the error with a human
            #  understandable message
            msg = "Failed to open() instrument communications: "\
                  "{!r}".format(Error(result))
            self.error(msg)
            raise ConnectionError(msg)
        self.__is_open = True
        self.debug("connection open, handler {}".format(self.__handle))
        self.__populate_instrument_properties()

    def __do_instrument_close(self):
        cdef:
            unsigned int result
        if self.is_open():
            self.debug("going to call Close to the handler {}"
                       "".format(self.__handle))
            result = SA_SI_Close(self.__handle)
            if result != SA_SI_OK:
                msg = "Close to the instrument failed: {}"\
                      "".format(Error(result))
                self.error(msg)
                raise ConnectionError(msg)
            self.info("connection close")
            self.__is_open = False
        else:
            self.warning("Nothing to close, it is not open")

    def __populate_instrument_properties(self):
        self.__get_instrument_id()
        self.__get_instrument_number_of_channels()
        self.__get_data_sources_per_channel()
        self.__get_streaming_mode()

    def __get_instrument_id(self):
        try:
            property_obj = InstrumentProperty(SA_SI_DEVICE_ID_PROP)
            instrument_id = self.__get_property_s(0, 0, property_obj)
            self.debug("instrument identified as '{}'"
                       "".format(instrument_id))
            self.__instrument_id = instrument_id
        except AssertionError or AttributeError as exc:
            self.error(
                "Cannot get the DEVICE_ID: {}".format(exc))
            raise exc

    @property
    def instrument_id(self):
        return self.__instrument_id

    def __get_instrument_number_of_channels(self):
        try:
            property_obj = InstrumentProperty(SA_SI_NUMBER_OF_CHANNELS_PROP)
            number_of_channels = self.__get_property_i32(0, 0, property_obj)
            self.debug("instrument reports {} channels"
                       "".format(number_of_channels))
            self.__nchannels = number_of_channels
        except AssertionError or AttributeError as exc:
            self.error(
                "Cannot get the NUMBER_OF_CHANNELS: {}".format(exc))
            raise exc

    @property
    def nchannels(self):
        return self.__nchannels

    def __get_data_sources_per_channel(self):
        n_data_sources_per_channel = []
        self.debug("Requesting data sources to each channel")
        for channel in range(self.__nchannels):
            try:
                property_obj = ChannelProperty(
                    SA_SI_NUMBER_OF_DATA_SOURCES_PROP)
                number_of_data_sources = self.__get_property_i32(
                    channel, 0, property_obj)
                self.debug(
                    "channel {} reports {} data sources"
                    "".format(channel, number_of_data_sources))
                n_data_sources_per_channel.append(number_of_data_sources)
            except AssertionError or AttributeError as exc:
                self.error(
                    "channel {} error when request data sources: {}"
                    "".format(channel, exc))
                n_data_sources_per_channel.append(None)
        self.debug("the {} report {} data sources each"
                   "".format(self.__nchannels, n_data_sources_per_channel))
        self.__n_data_sources_per_channel = n_data_sources_per_channel

    @property
    def n_data_sources_per_channel(self):
        return self.__n_data_sources_per_channel

    def __get_streaming_mode(self):
        self.__streaming_mode = StreamingMode(SA_SI_DIRECT_STREAMING)

    def __check_channel_bounds(self, channel):
        if channel < 0 or channel > self.__nchannels:
            msg = "channel {} is out of range [0..{}]".format(
                channel, self.__nchannels)
            self.error(msg)
            raise IndexError(msg)

    def __check_data_source_bounds(self, channel, data_source_index):
        if data_source_index < 0 or \
                data_source_index > self.__n_data_sources_per_channel[channel]:
            msg = "data source {} of channel {} is out of range [0..{}]"\
                  "".format(data_source_index, channel,
                            self.__n_data_sources_per_channel)
            self.error(msg)
            raise IndexError(msg)

    def is_streamable(self, int channel, int data_source_index):
        self.__check_channel_bounds(channel)
        self.__check_data_source_bounds(channel, data_source_index)
        try:
            property_obj = DataSourceProperty(SA_SI_IS_STREAMABLE_PROP)
            value = bool(self.__get_property_i32(
                channel, data_source_index, property_obj))
            self.debug(
                "channel {}, data source {} {} is {}".format(
                    channel, data_source_index, property_obj.human,
                    "is streamable" if value else "is NOT streamable"))
            return value
        except AssertionError or AttributeError as exc:
            self.error(
                "Error on channel {}, data source {}: "
                "{}".format(channel, data_source_index, exc))
            raise exc

    def is_streaming_enabled(self, int channel, int data_source_index):
        if self.is_streamable(channel, data_source_index):
            try:
                property_obj = DataSourceProperty(SA_SI_STREAMING_ENABLED_PROP)
                value = bool(self.__get_property_i32(
                    channel, data_source_index, property_obj))
                self.debug(
                    "channel {}, data source {} {} is {}".format(
                        channel, data_source_index, property_obj.human,
                        "enabled" if value else "disabled"))
                return value
            except AssertionError or AttributeError as exc:
                self.error(
                    "Error on channel {}, data source {}: "
                    "{}".format(channel, data_source_index, exc))
                raise exc

    def get_data_source_type(self, int channel, int data_source_index):
        try:
            property_obj = DataSourceProperty(SA_SI_DATA_SOURCE_TYPE_PROP)
            value = DataType(self.__get_property_i32(
                channel, data_source_index, property_obj))
            self.debug(
                "channel {}, data source {} {} is {!r}".format(
                    channel, data_source_index, property_obj.human, value))
            return value
        except AssertionError or AttributeError as exc:
            self.error(
                "Error on channel {}, data source {}: "
                "{}".format(channel, data_source_index, exc))
            raise exc

    def get_data_type(self, int channel, int data_source_index):
        """
The format in which data values are transmitter from unit that controls the
sensors to the information requester.
        """
        try:
            property_obj = DataSourceProperty(SA_SI_DATA_TYPE_PROP)
            value = DataType(self.__get_property_i32(
                channel, data_source_index, property_obj))
            self.debug(
                "channel {}, data source {} {} is {!r}".format(
                    channel, data_source_index, property_obj.human, value))
            return value
        except AssertionError or AttributeError as exc:
            self.error(
                "Error on channel {}, data source {}: "
                "{}".format(channel, data_source_index, exc))
            raise exc

    def get_compression_modes(self, int channel, int data_source_index):
        # TODO: use CompressModes to interpret it
        try:
            property_obj = DataSourceProperty(
                SA_SI_AVAILABLE_COMPRESSION_MODES_PROP)
            value = self.__get_property_i32(
                channel, data_source_index, property_obj, array_size=8)
            self.debug(
                "channel {}, data source {} {} is {}".format(
                    channel, data_source_index, property_obj.human, value))
            return value
        except AssertionError or AttributeError as exc:
            self.error(
                "Error on channel {}, data source {}: "
                "{}".format(channel, data_source_index, exc))
            raise exc

    def get_compression_mode(self, int channel, int data_source_index):
        # TODO: use CompressModes to interpret it
        try:
            property_obj = DataSourceProperty(SA_SI_COMPRESSION_MODE_PROP)
            value = self.__get_property_i32(
                channel, data_source_index, property_obj)
            self.debug(
                "channel {}, data source {} {} is {}".format(
                    channel, data_source_index, property_obj.human, value))
            return value
        except AssertionError or AttributeError as exc:
            self.error(
                "Error on channel {}, data source {}: "
                "{}".format(channel, data_source_index, exc))
            raise exc

    def get_base_unit(self, int channel, int data_source_index):
        try:
            property_obj = DataSourceProperty(SA_SI_BASE_RESOLUTION_PROP)
            value = Units(self.__get_property_i32(
                channel, data_source_index, property_obj))
            self.debug(
                "channel {}, data source {} {} is {!r}".format(
                    channel, data_source_index, property_obj.human, value))
            return value
        except AssertionError or AttributeError as exc:
            self.error(
                "Error on channel {}, data source {}: "
                "{}".format(channel, data_source_index, exc))
            raise exc

    def get_base_resolution(self, int channel, int data_source_index):
        try:
            property_obj = DataSourceProperty(SA_SI_BASE_RESOLUTION_PROP)
            value = self.__get_property_i32(
                channel, data_source_index, property_obj)
            self.debug(
                "channel {}, data source {} {} is {}".format(
                    channel, data_source_index, property_obj.human, value))
            return value
        except AssertionError or AttributeError as exc:
            self.error(
                "Error on channel {}, data source {}: "
                "{}".format(channel, data_source_index, exc))
            raise exc

    ################################
    # lower level property getters

    def __get_property_s(self, int p1, int p2, _Property property_obj):
        cdef:
            unsigned int result
            char buffer[256]  # FIXME: avoid this hardcoded size
            unsigned int bufferSize
        if not self.is_open():
            msg = "instrument communications are not open"
            raise AssertionError(msg)
        bufferSize = sizeof(buffer)
        result = SA_SI_GetProperty_s(
            self.__handle, SA_SI_EPK(int(property_obj), p1, p2),
            buffer, &bufferSize)
        if result == SA_SI_OK:
            self.debug(
                "{}:{} property {!r} is {}".format(
                    hex(p1), hex(p2), property_obj, <bytes> buffer))
            return <bytes> buffer
        else:
            raise AttributeError(
                "Cannot get the {!r} property for {}:{} {!r}"
                "".format(property_obj, hex(p1), hex(p2), Error(result)))

    def __get_property_i32(self, int p1, int p2, _Property property_obj,
                           array_size=0):
        cdef:
            unsigned int result
            int32_t* _value
            unsigned int _size
        if not self.is_open():
            msg = "instrument communications are not open"
            raise AssertionError(msg)
        _size = array_size
        _value = <int32_t*> PyMem_Malloc(_size * sizeof(int32_t))
        if not _value:
            raise MemoryError()
        if array_size == 0:
            result = SA_SI_GetProperty_i32(
                self.__handle, SA_SI_EPK(int(property_obj), p1, p2),
                _value, NULL)
        else:
            self.debug("...")
            result = SA_SI_GetProperty_i32(
                self.__handle, SA_SI_EPK(int(property_obj), p1, p2),
                _value, &_size)
        if result == SA_SI_OK:
            if array_size == 0:
                value = _value[0]
            else:
                value = [_value[i] for i in range(array_size)]
            self.debug(
                "{}:{} property {} is {!r}".format(
                    hex(p1), hex(p2), property_obj, value))
            PyMem_Free(_value)
            return value
        else:
            PyMem_Free(_value)
            error_obj = Error(result)
            raise AttributeError(
                "Cannot get the {!r} property for {}:{}:{} {}"
                "".format(property_obj, hex(p1), hex(p2), array_size,
                          error_obj))

    # FIXME: those methods should be private and have an interface method to
    #  access specifically the information required
    def get_value_i64(self, int channel, int source, array_size=0):
    
        cdef:
            unsigned int result
            int64_t* _value
            unsigned int _size
        if not self.is_open():
            msg = "instrument communications are not open"
            raise AssertionError(msg)
        _value = <int64_t*> PyMem_Malloc(sizeof(int64_t))
        if not _value:
            raise MemoryError()
        result = SA_SI_GetValue_i64(self.__handle, channel, source, _value)
        if result == SA_SI_OK:
            value = _value[0]
            PyMem_Free(_value)
            return value
        else:
            PyMem_Free(_value)
            error_obj = Error(result)
            raise AttributeError(
                "Cannot get the i64 value for channel {} source {}: {}"
                "".format(channel, source, error_obj))

    def get_value_f64(self, int channel, int source, array_size=0):
    
        cdef:
            unsigned int result
            double* _value
            unsigned int _size
        if not self.is_open():
            msg = "instrument communications are not open"
            raise AssertionError(msg)
        _value = <double*> PyMem_Malloc(sizeof(double))
        if not _value:
            raise MemoryError()
        result = SA_SI_GetValue_f64(self.__handle, channel, source, _value)
        if result == SA_SI_OK:
            value = _value[0]
            PyMem_Free(_value)
            return value
        else:
            PyMem_Free(_value)
            error_obj = Error(result)
            raise AttributeError(
                "Cannot get the i64 value for channel {} source {}: {}"
                "".format(channel, source, error_obj))
