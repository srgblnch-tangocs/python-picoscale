# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

include "smaract_h.pxi"

from . cimport code

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2020, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

cdef class Error(code.Code):
    _valid_values = {
        SA_SI_OK: ["OK", "no error"],
        SA_SI_UNKNOWN_COMMAND_ERROR: ["UNKNOWN_COMMAND", "Unknown command"],
        SA_SI_PACKET_SIZE_ERROR: ["PACKET_SIZE", "Packet size error"],
        SA_SI_TIMEOUT_ERROR: ["TIMEOUT", "Timeout error"],
        SA_SI_PROTOCOL_ERROR: ["PROTOCOL", "Protocol error"],
        SA_SI_BUFFER_UNDERFLOW_ERROR: ["BUFFER_UNDERFLOW", "Buffer underflow"],
        SA_SI_BUFFER_OVERFLOW_ERROR: ["BUFFER_OVERFLOW", "Buffer overflow"],
        SA_SI_INVALID_PACKET_ERROR: ["INVALID_PACKET", "Invalid packet"],
        SA_SI_INVALID_STREAM_PACKET_ERROR: ["INVALID_STREAM_PACKET",
                                            "Invalid packet stream"],
        SA_SI_INVALID_PROPERTY_ERROR: ["INVALID_PROPERTY", "Invalid property"],
        SA_SI_INVALID_PARAMETER_ERROR: ["INVALID_PARAMETER",
                                        "Invalid parameter"],
        SA_SI_INVALID_CHANNEL_INDEX_ERROR: ["INVALID_CHANNEL_INDEX",
                                            "Invalid channel index"],
        SA_SI_INVALID_DSOURCE_INDEX_ERROR: ["INVALID_DSOURCE_INDEX",
                                            "Invalid data source index"],
        SA_SI_INVALID_DATA_TYPE_ERROR: ["INVALID_DATA_TYPE",
                                        "Invalid data type"],
        SA_SI_PERMISSION_DENIED_ERROR: ["PERMISSION_DENIED",
                                        "Access violation error"],
        SA_SI_NO_DATA_SOURCES_ENABLED_ERROR: ["NO_DATA_SOURCES_ENABLED",
                                              "No data sources enabled"],
        SA_SI_STREAMING_ACTIVE_ERROR: ["STREAMING_ACTIVE",
                                       "Streaming is active"],
        SA_SI_DATA_SOURCE_NOT_STREAMABLE_ERROR: ["DATA_SOURCE_NOT_STREAMABLE",
                                                 "Data source not streamable"],
        SA_SI_UNKNOWN_DATA_OBJECT_ERROR: ["UNKNOWN_DATA_OBJECT",
                                          "Unknown data object"],
        SA_SI_COMMAND_NOT_PROCESSABLE_ERROR: ["COMMAND_NOT_PROCESSABLE",
                                              "Command cannot be processed"],
        SA_SI_FEATURE_NOT_SUPPORTED_ERROR: ["FEATURE_NOT_SUPPORTED",
                                            "Feature not supported"],
        SA_SI_NOT_IMPLEMENTED_ERROR: ["NOT_IMPLEMENTED",
                                      "Feature not implemented"],
        SA_SI_OTHER_ERROR: ["OTHER", "Uncategorized error"],
        0xf004: ["UNKNOWN_ERROR",
                 "undocumented error "
                 "(check if there is another software controlling it)"],
    }
