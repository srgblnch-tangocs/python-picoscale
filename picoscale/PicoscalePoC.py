#!/usr/bin/env python

from tango.server import run
from tango.server import Device
from tango.server import device_property
from tango.server import attribute

from picoscale import Picoscale

class PicoscalePoC(Device):

    pscdeviceip = device_property(dtype=str)
    # pscchannel = device_property(dtype=int)

    def init_device(self):
        self.info_stream('init_device')
        Device.init_device(self)
        self.psc = Picoscale(ip_address=self.pscdeviceip)
        # self.ch = self.pscchannel

    def delete_device(self):
        self.psc.close()

    ch1_position = attribute(dtype=float)
    ch1_position_int = attribute(dtype=int)
    ch2_position = attribute(dtype=float)
    ch2_position_int = attribute(dtype=int)
    ch3_position = attribute(dtype=float)
    ch3_position_int = attribute(dtype=int)

    def read_ch1_position(self):
        return self.psc.get_value_f64(0, 0)

    def read_ch1_position_int(self):
        return self.psc.get_value_i64(0, 0)

    def read_ch2_position(self):
        return self.psc.get_value_f64(1, 0)

    def read_ch2_position_int(self):
        return self.psc.get_value_i64(1, 0)

    def read_ch3_position(self):
        return self.psc.get_value_f64(2, 0)

    def read_ch3_position_int(self):
        return self.psc.get_value_i64(2, 0)

def main():
    run((PicoscalePoC,))

if __name__ == "__main__":
    main()
