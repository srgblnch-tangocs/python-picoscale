# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2020, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

include "stdint_h.pxi"
include "stdlib_h.pxi"
include "smaract_h.pxi"

cdef extern from "SmarActSI.h" nogil:
    ctypedef unsigned int SA_SI_Handle
    ctypedef uint32_t SA_SI_PropertyKey

    unsigned int SA_SI_Open(SA_SI_Handle *handle,const char *locator,
                            const char *config)
    unsigned int SA_SI_Close(SA_SI_Handle handle)

    # properties:
    unsigned int SA_SI_GetProperty_s(SA_SI_Handle handle,SA_SI_PropertyKey pk,
                                     char *value,unsigned int *ioArraySize)
    unsigned int SA_SI_GetProperty_i32(SA_SI_Handle handle,
                                       SA_SI_PropertyKey pk, int32_t *value,
                                       unsigned int *ioArraySize)

    # values:
    unsigned int SA_SI_GetValue_i64(SA_SI_Handle handle, uint8_t channel,
                                    uint8_t source , int64_t *value)
    unsigned int SA_SI_GetValue_f64(SA_SI_Handle handle, uint8_t channel,
                                    uint8_t source , double *value)

    #
    SA_SI_PropertyKey SA_SI_EPK(uint16_t prop, uint8_t p1, uint8_t p2)


DEFAULT_PORT = 55555
