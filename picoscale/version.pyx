# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2020, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

include "version.pxi"

# Look at https://en.wikipedia.org/wiki/Software_versioning

# we use semantic versioning (http://semver.org/) and we update it using the
# bumpversion script (https://github.com/peritus/bumpversion)

__version__ = '0.0.1-alpha3'

def python_module():
    """Semantic versioning of this PicoScale python module in a single string.
    """
    return str.encode(__version__)


def python_module_tuple():
    """Tuple structure with the semantic version of this PicoScale python module.
    """
    if '-' in __version__:
        _v, _rel = __version__.split('-')
    else:
        _v, _rel = __version__, ''
    _v = [int(n) for n in _v.split('.')]
    if len(_rel) > 0:
        _v += [_rel]
    return tuple(_v)

def smaract_picoscale_sdk():
    return _smaract_picoscale_fullversionstring()

def smaract_picoscale_sdk_tuple():
    return tuple(_smaract_picoscale_fullversionstring().decode().split('.'))

# TODO api version
cdef _smaract_picoscale_fullversionstring():
    cdef:
        const char* answer
    answer = SA_SI_GetFullVersionString();
    return answer
