# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2020, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

cdef extern from "SmarActSIConstants.h" nogil:
    cdef int SA_SI_TIMEOUT_INFINITE     
    cdef int SA_SI_DISABLED             
    cdef int SA_SI_ENABLED              
    cdef int SA_SI_STRING_MAX_LENGTH    

    # Error codes
    cdef int SA_SI_OK
    cdef int SA_SI_UNKNOWN_COMMAND_ERROR
    cdef int SA_SI_PACKET_SIZE_ERROR
    cdef int SA_SI_TIMEOUT_ERROR
    cdef int SA_SI_PROTOCOL_ERROR
    cdef int SA_SI_BUFFER_UNDERFLOW_ERROR
    cdef int SA_SI_BUFFER_OVERFLOW_ERROR
    cdef int SA_SI_INVALID_PACKET_ERROR
    cdef int SA_SI_INVALID_STREAM_PACKET_ERROR
    cdef int SA_SI_INVALID_PROPERTY_ERROR
    cdef int SA_SI_INVALID_PARAMETER_ERROR
    cdef int SA_SI_INVALID_CHANNEL_INDEX_ERROR
    cdef int SA_SI_INVALID_DSOURCE_INDEX_ERROR
    cdef int SA_SI_INVALID_DATA_TYPE_ERROR
    cdef int SA_SI_PERMISSION_DENIED_ERROR
    cdef int SA_SI_NO_DATA_SOURCES_ENABLED_ERROR
    cdef int SA_SI_STREAMING_ACTIVE_ERROR
    cdef int SA_SI_DATA_SOURCE_NOT_STREAMABLE_ERROR
    cdef int SA_SI_UNKNOWN_DATA_OBJECT_ERROR
    cdef int SA_SI_COMMAND_NOT_PROCESSABLE_ERROR
    cdef int SA_SI_FEATURE_NOT_SUPPORTED_ERROR
    cdef int SA_SI_NOT_IMPLEMENTED_ERROR
    cdef int SA_SI_OTHER_ERROR

    # data types
    cdef int SA_SI_INT8_DTYPE       
    cdef int SA_SI_UINT8_DTYPE      
    cdef int SA_SI_INT16_DTYPE      
    cdef int SA_SI_UINT16_DTYPE     
    cdef int SA_SI_INT32_DTYPE      
    cdef int SA_SI_UINT32_DTYPE     
    cdef int SA_SI_INT48_DTYPE      
    cdef int SA_SI_UINT48_DTYPE     
    cdef int SA_SI_INT64_DTYPE      
    cdef int SA_SI_UINT64_DTYPE     
    cdef int SA_SI_FLOAT32_DTYPE    
    cdef int SA_SI_FLOAT64_DTYPE    
    cdef int SA_SI_STRING_DTYPE

    # base unit types
    cdef int SA_SI_NO_UNIT                     
    cdef int SA_SI_PERCENT_UNIT                
    cdef int SA_SI_METRE_UNIT                  
    cdef int SA_SI_DEGREE_UNIT                 
    cdef int SA_SI_SECOND_UNIT                 
    cdef int SA_SI_HERTZ_UNIT                  
    cdef int SA_SI_KILOGRAM_UNIT               
    cdef int SA_SI_NEWTON_UNIT                 
    cdef int SA_SI_WATT_UNIT                   
    cdef int SA_SI_JOULE_UNIT                  
    cdef int SA_SI_VOLT_UNIT                   
    cdef int SA_SI_AMPERE_UNIT                 
    cdef int SA_SI_OHM_UNIT                    
    cdef int SA_SI_PASCAL_UNIT                 
    cdef int SA_SI_KELVIN_UNIT                 
    cdef int SA_SI_DEGREE_CELSIUS_UNIT         
    cdef int SA_SI_SQUARE_METRE_UNIT           
    cdef int SA_SI_METRE_PER_SECOND_UNIT       
    cdef int SA_SI_METRE_PER_SQUARE_SECOND_UNIT

    # data source types
    cdef int SA_SI_ANALOG_RAW_DSOURCE        
    cdef int SA_SI_SIN_RAW_DSOURCE           
    cdef int SA_SI_COS_RAW_DSOURCE           
    cdef int SA_SI_SIN_QUALITY_DSOURCE       
    cdef int SA_SI_COS_QUALITY_DSOURCE       
    cdef int SA_SI_SIN_CORRECTED_DSOURCE     
    cdef int SA_SI_COS_CORRECTED_DSOURCE     
    cdef int SA_SI_POSITION_DSOURCE          
    cdef int SA_SI_STATUS_DSOURCE            
    cdef int SA_SI_TEMPERATURE_DSOURCE       
    cdef int SA_SI_HUMIDITY_DSOURCE          
    cdef int SA_SI_PRESSURE_DSOURCE          
    cdef int SA_SI_VELOCITY_DSOURCE          
    cdef int SA_SI_ACCELERATION_DSOURCE      
    cdef int SA_SI_COUNTER_DSOURCE           
    cdef int SA_SI_GENERIC_DSOURCE           
    cdef int SA_SI_AMPLITUDE_DSOURCE         
    cdef int SA_SI_FREQUENCY_DSOURCE         
    cdef int SA_SI_PHASE_DSOURCE             
    cdef int SA_SI_RADIUS_DSOURCE            
    cdef int SA_SI_ANGLE_DSOURCE             

    # compression modes
    cdef int SA_SI_NO_COMPRESSION_MODE           
    cdef int SA_SI_LOWER16ABS_COMPRESSION_MODE   

    # event types
    cdef int SA_SI_STREAM_ABORTED_EVENT                  
    cdef int SA_SI_DATA_SOURCE_INFO_CHANGED_EVENT        

    # streaming modes
    cdef int SA_SI_NO_STREAMING
    cdef int SA_SI_DIRECT_STREAMING
    cdef int SA_SI_TRIGGERED_STREAMING

    # open modes
    cdef int SA_SI_READ_MODE           
    cdef int SA_SI_WRITE_MODE          

    # connection options
    cdef int SA_SI_CRC_MODE_OPTION                           

    # stream stopped reason codes
    cdef int SA_SI_STOPPED_BY_USER            
    cdef int SA_SI_STOPPED_BY_TRIGGER         
    cdef int SA_SI_STOPPED_BY_BUFFER_OVERFLOW 

    # properties
    ## device properties
    cdef int SA_SI_PROTOCOL_VERSION_PROP
    cdef int SA_SI_PROTOCOL_VERSION_STRING_PROP
    cdef int SA_SI_DEVICE_TYPE_PROP
    cdef int SA_SI_DEVICE_ID_PROP
    cdef int SA_SI_DEVICE_SERIAL_NUMBER_PROP
    cdef int SA_SI_DEVICE_NAME_PROP
    cdef int SA_SI_NUMBER_OF_FIRMWARE_VERSIONS_PROP
    cdef int SA_SI_FIRMWARE_VERSION_PROP
    cdef int SA_SI_FIRMWARE_VERSION_STRING_PROP
    cdef int SA_SI_MAX_DATA_OBJECT_CHUNK_SIZE_PROP
    cdef int SA_SI_NUMBER_OF_CHANNELS_PROP
    cdef int SA_SI_MAX_FRAME_RATE_PROP
    cdef int SA_SI_FRAME_RATE_PROP
    cdef int SA_SI_MAX_FRAME_AGGREGATION_PROP
    cdef int SA_SI_FRAME_AGGREGATION_PROP
    cdef int SA_SI_FRAME_INDEX_ENABLED_PROP
    cdef int SA_SI_PRECISE_FRAME_RATE_PROP
    cdef int SA_SI_EVENT_NOTIFICATION_ENABLED_PROP
    cdef int SA_SI_STREAMING_ACTIVE_PROP
    cdef int SA_SI_STREAMING_MODE_PROP
    ## channel properties
    cdef int SA_SI_NUMBER_OF_DATA_SOURCES_PROP
    cdef int SA_SI_CHANNEL_NAME_PROP
    ## data source properties
    cdef int SA_SI_DATA_SOURCE_TYPE_PROP
    cdef int SA_SI_DATA_TYPE_PROP
    cdef int SA_SI_AVAILABLE_COMPRESSION_MODES_PROP
    cdef int SA_SI_COMPRESSION_MODE_PROP
    cdef int SA_SI_STREAMING_ENABLED_PROP
    cdef int SA_SI_BASE_UNIT_PROP
    cdef int SA_SI_BASE_RESOLUTION_PROP
    cdef int SA_SI_RESOLUTION_SHIFT_PROP
    cdef int SA_SI_DATA_SOURCE_NAME_PROP
    cdef int SA_SI_IS_STREAMABLE_PROP
    cdef int SA_SI_COMPONENT_ID_PROP
    cdef int SA_SI_COMPONENT_INDEX_PROP
