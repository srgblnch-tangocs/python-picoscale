# SmarAct PicoScale

Python3 module to port the Manufaturer's SDK using Cython

## ToDo list:

- [ ] ...

## Install

Prerequisite _[Cython](https://cython.org/)_ (used 0.29.17 from pip3 during the development, 0.25.2 in debian9/stretch) and setuptools (used 40.8.0-1 in debian10/buster and 33.1.1-1 in debian9/stretch)

It requires to have installed the _privative_ SDK from the manufacturer (used their release 2.2.5 during the development, that say 2.0.9 when ask the api).

Tested using Python 3.5.3 (debian9/stretch) and 3.7.3 (debian10/buster).

```
python3 setup.py build
sudo python3 setup.py install
```

## Usage

```python
>>> from picoscale import version as picoscale_version
>>> picoscale_version.python_module()
 b'0.0.1-alpha0'
>>> picoscale_version.smaract_picoscale_sdk()
 b'2.0.9.67120'
>>> from picoscale import Picoscale
>>> instrument = Picoscale(ip_address='...')
>>> instrument.instrument_id
 b'PSC-00000164'
>>> x.nchannels
 3
```
