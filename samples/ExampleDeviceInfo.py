# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

import argparse
from picoscale import Picoscale
from picoscale import version as picoscale_version
import traceback


__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2020, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

__description__ = \
    "Test the python module for the Picoscale by replicate the first of the " \
    "examples the c library provide"

def main():
    print("Test using python api version {} and c api version {}".format(
        picoscale_version.python_module(),
        picoscale_version.smaract_picoscale_sdk()))
    parser = argparse.ArgumentParser(description=__description__)
    parser.add_argument(
        "ip", metavar='IP',
        help="The ip address of the instrument to use in the test")
    args = parser.parse_args()
    instrument = Picoscale(ip_address=args.ip, debug=True)
    print("Intrument id: {}".format(instrument.instrument_id))
    print("Instrument number of channels: {}".format(instrument.nchannels))
    print("Instrument data sources per channel: {}".format(
        instrument.n_data_sources_per_channel))
    for ch in range(instrument.nchannels):
        for data_source in range(instrument.n_data_sources_per_channel[ch]):
            if not instrument.is_streamable(ch, data_source):
                print(
                    "Channel {:2d} data source {:2d} is not streamable".format(
                        ch, data_source))
            elif not instrument.is_streaming_enabled(ch, data_source):
                print(
                    "Channel {:2d} data source {:2d} hasn't the stream enable"
                    "".format(ch, data_source))
            else:
                print("Channel {:2d} data source {:2d} data source type is {}"
                      "".format(ch, data_source,
                                instrument.get_data_source_type(
                                    ch, data_source)))
                print("Channel {:2d} data source {:2d} data type is {}"
                      "".format(ch, data_source,
                                instrument.get_data_type(ch, data_source)))
            modes = instrument.get_compression_modes(ch, data_source)
            current_mode = instrument.get_compression_mode(ch, data_source)
            print(
                "Channel {:2d} data source {:2d} compression modes are "
                "{}".format(
                    ch, data_source,
                    ["{} (active)".format(hex(mode))
                     if mode == current_mode else "{}".format(hex(mode))
                     for mode in modes]))
            print("Channel {:2d} data source {:2d} base unit is {}"
                  "".format(ch, data_source,
                            instrument.get_base_unit(ch, data_source)))
            print("Channel {:2d} data source {:2d} base resolution is {}"
                  "".format(ch, data_source,
                            instrument.get_base_resolution(ch, data_source)))


if __name__ == '__main__':
    main()